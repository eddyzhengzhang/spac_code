#include <vector>
#include <iostream>
#include <fstream>
#include <cstring>
#include <set>
#include <stdlib.h>
#include <assert.h>
#include <metis.h>
#include <math.h>
#include <string>
#include <sstream>
using namespace std;

int subtractOne = 1;

typedef struct Edge {
	int head;
	int tail;
} Edge;

//This hopefully lets the set class handle Edge pointers properly:
typedef struct EdgeComp
{
	bool operator()(const Edge* lhs, const Edge* rhs) const {
		return lhs->head < rhs->head || (lhs->head == rhs->head && lhs->tail < rhs->tail);
	}
} EdgeComp;

int * vertexpart(Edge * edges, int numvertices, int numedges, int numparts) {
	clock_t Tend, Tbegin;
	double time_spent;
	Tbegin = clock();
	
	//Allocate main metis inputs:
	idx_t * xadj = (idx_t *) malloc((numvertices + 1) * sizeof(idx_t));
	idx_t * adjncy = (idx_t *) malloc(2 * numedges * sizeof(idx_t));
	
	//Generate edge set to make it easier to count, avoiding issues where edge list might contain both (x,y) and (y,x):
	set<Edge*, EdgeComp> edgeSet;
	Edge * edgeSetMem = (Edge *) malloc(numedges * sizeof(Edge));//so I don't have waste memops allocating every Edge separately
	for(int edge = 0; edge < numedges; edge++) {
		int x = edges[edge].head;
		int y = edges[edge].tail;
		if(x > y) {
			int temp = x;
			x = y;
			y = temp;
		}
		edgeSetMem[edge].head = x;
		edgeSetMem[edge].tail = y;
		edgeSet.insert(&edgeSetMem[edge]);
	}
	
	//Count edges per vertex in linear time:
	int * vertexCounters = (int *) calloc(numvertices, sizeof(int));
	std::set<Edge*>::iterator it;
	for(it = edgeSet.begin(); it != edgeSet.end(); it++) {
		Edge * e = *it;
		int head = e->head;
		int tail = e->tail;
		vertexCounters[head]++;
		if(head != tail) {
			vertexCounters[tail]++;
		}
	}
	
	//Fill in xadj list in linear time:
	int tempsum = 0;
	for(int vertex = 0; vertex < numvertices; vertex++) {
		xadj[vertex] = tempsum;
		tempsum += vertexCounters[vertex];
	}
	xadj[numvertices] = tempsum;
	
	//Fill in adjncy list in linear time:
	memset(vertexCounters, 0, numvertices * sizeof(int));
	for(it = edgeSet.begin(); it != edgeSet.end(); it++) {
		Edge * e = *it;
		int head = e->head;
		int tail = e->tail;
		
		int loc = vertexCounters[head] + xadj[head];
		adjncy[loc] = tail;
		vertexCounters[head]++;
		
		if(head != tail) {
			loc = vertexCounters[tail] + xadj[tail];
			adjncy[loc] = head;
			vertexCounters[tail]++;
		}
	}
	
	/*
	for(int x = 0; x < numvertices + 1; x++) {
		cerr << xadj[x] << ", ";
	}
	cerr << "\n";
	for(int x = 0; x < numedges; x++) {
		cerr << adjncy[x] << ", ";
	}
	cerr << "\n";
	*/
	
	//Free variables we no longer need:
	edgeSet.clear();
	free(edgeSetMem);
	free(vertexCounters);
	
	Tend = clock();
	time_spent = (double)(Tend - Tbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::Prepare input for metis: %lf\n", time_spent);
	
	Tbegin = clock();
	
	//Some more parameters for metis function
	idx_t nVertices = numvertices;
    idx_t nWeights  = 1;
    idx_t nParts = numparts;
    idx_t objval;
    idx_t * part = (idx_t*)malloc(sizeof(idx_t)*nVertices);
	
	//Run metis:
	idx_t ret = METIS_PartGraphKway(&nVertices,&nWeights, xadj, adjncy, NULL, NULL, NULL, &nParts, NULL, NULL, NULL, &objval, part);
	if(ret != METIS_OK) {
		cerr << "ERROR: metis threw an error.\n";
	}
	
	Tend = clock();
	time_spent = (double)(Tend - Tbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::metis: %lf\n", time_spent);
	
	//Free metis inputs that we no longer need:
	free(xadj);
	free(adjncy);
	
	return part;
}

int main(int argc, char *argv[]) {
	if(argc < 3) {
		cerr << "Error (fatak): expects filepath and partition number.\n";
		exit(1);
	}
	
	FILE *fp;
	
	char * matpath = argv[1];
	int numparts = atoi(argv[2]);
	
	//Open new graph file
    if ((fp = fopen(matpath, "r")) == NULL) {
      cerr << "ERROR (fatal) Unable to open input new graph file\n";
      exit(1);
    }
	
	char line[1024];
	if(!fgets(line, 1023, fp)) {
		cerr << "ERROR (fatal) ~27 reading file.\n";
		exit(1);
	}
	int numvertices;
	int numedges;
	if(sscanf(line, "%d %d", &numvertices, &numedges) < 2) {
		cerr << "ERROR (fatal) ~32 could not get header.\n";
		exit(1);
	}
	numedges *= 2;
	
	Edge * edges = (Edge *) malloc(sizeof(Edge) * numedges);
	
	int edgesSeen = 0;
	int currentVertex = 0;
	while(true) {
		if(!fgets(line, 1023, fp)) {
			break;
		}
		
		int neighborsSeen = 0;
		
		//Read in all neighbors for current vertex:
		string str(line);
		istringstream sstr(str);
		int next;
		while(sstr >> next) {
			edges[edgesSeen].head = currentVertex;
			edges[edgesSeen].tail = next - subtractOne;
			
			edgesSeen++;
			neighborsSeen++;
		}
		
		/*
		if(false) {
			int start = edgesSeen - neighborsSeen;
			int end = edgesSeen;
			for(int x = start; x < end - 1; x++) {
				for(int y = x + 1; y < end; y++) {
					if(edges[x].tail < edges[y].tail) {
						int temp = edges[x].tail;
						edges[x].tail = edges[y].tail;
						edges[y].tail = temp;
					}
				}
			}
		}
		*/
		
		currentVertex++;
	}
	
	if(edgesSeen != numedges) {
		cerr << "ERROR ~43 edge count mismatch.\n";
	}
	
	//Call metis caller:
	int * output;
	output = vertexpart(edges, numvertices, edgesSeen, numparts);
	
	//Write output to stdout
	for(int x = 0; x < numvertices; x++) {
		cout << output[x] << "\n";
	}
	
	return 0;
}
