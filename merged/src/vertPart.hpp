#ifndef VERTPART_HPP
#define VERTPART_HPP

typedef struct Edge {
	int head;
	int tail;
} Edge;

//This hopefully lets the set class handle Edge pointers properly:
typedef struct EdgeComp
{
	bool operator()(const Edge* lhs, const Edge* rhs) const {
		return lhs->head < rhs->head || (lhs->head == rhs->head && lhs->tail < rhs->tail);
	}
} EdgeComp;


int * vertexpart(Edge * edges, int numvertices, int numedges, int numparts);

#endif