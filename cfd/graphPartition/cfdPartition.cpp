#include <iostream>
#include <stdlib.h>
#include <common.hpp>
#include <edgepart.hpp>
#include <vertexpart.hpp>
#include <fstream>

#define NNB 4
#define NDIM 3

using namespace std;

int main(int argc, char *argv[]) {

	char *inputFileName = argv[1];
	char *outputFileName = argv[2];
	int partNum = atoi(argv[3]);
	char * outputInfoName = argv[4];
		
	// Read cfd data
	ifstream fin;
	fin.open(inputFileName);

	int nodeNum = 0;
	fin >> nodeNum;

	float *area = (float *) malloc(nodeNum * sizeof(float));
	int *neighbor = (int *) malloc(nodeNum * NNB * sizeof(int));
	float *normals = (float *) malloc(nodeNum * NDIM * NNB * sizeof(float));
	int edgeNum = 0;

	for (int i = 0; i < nodeNum; i++) {
		fin >> area[i];
		
		for (int j = 0; j < NNB; ++j) {
			fin >> neighbor[i + j * nodeNum];
			neighbor[i + j * nodeNum]--;
			if (neighbor[i + j * nodeNum] >= 0) {
				edgeNum++;
			}
			for (int k = 0; k < NDIM; ++k) {
				fin >> normals[i + (j + k * NNB) * nodeNum];
			}
		}

	}

	fin.close();

	// Generate the graph from cfd
	Edge *edges = (Edge *)malloc(edgeNum * sizeof(Edge));
	int index = 0;
	for (int i = 0; i < nodeNum; ++i) {
		for (int j = 0; j < NNB; ++j) {
			if (neighbor[i + j * nodeNum] >= 0) {
				edges[index].head = i;
				edges[index].tail = neighbor[i + j * nodeNum];
				index++;
			}	
		}
	}

	cout << "# of edges: " << edgeNum << "\n# of vectices:  " << nodeNum << endl;


	int vertexNum = nodeNum;

	// Do vertex partitioning
	int *vertices;
	int *vPartSizes = vertexpart(edges, vertexNum, edgeNum, partNum, vertices);

	/*
	// Print Result
	cout << "Vertex partitions\n";
	int sizesum = 0;
	for (int part = 0; part < partNum; ++part) {
		for (int offset = 0; offset < vPartSizes[part]; ++offset) {
			cout << vertices[sizesum + offset] << " ";
		}
		cout << endl;
		sizesum += vPartSizes[part];
	}
	*/

	// Reorder input data
	float *newArea = (float *) malloc(nodeNum * sizeof(float));
	int *newNeighbor = (int *) malloc(nodeNum * NNB * sizeof(int));
	float *newNormals = (float *) malloc(nodeNum * NDIM * NNB * sizeof(float));

	int *nodeTable = (int *) malloc(nodeNum * sizeof(int));
	for (int i = 0; i < nodeNum; ++i) {
		nodeTable[vertices[i]] = i;
	}

	for (int i = 0; i < nodeNum; ++i) {

		newArea[i] = area[vertices[i]];
		
		for (int j = 0; j < NNB; ++j) {
			int oriNbNode = neighbor[vertices[i] + j * nodeNum];
			int nbNode;
			if (oriNbNode < 0) 
				nbNode = oriNbNode + 1;
			else
				nbNode = nodeTable[oriNbNode] + 1;

			newNeighbor[i + j * nodeNum] = nbNode;

			for (int k = 0; k < NDIM; ++k) {
				newNormals[i + (j + k * NNB) * nodeNum] = normals[vertices[i] + (j + k * NNB) * nodeNum];
			}
		}
	
	}

	// Write to new data file
	ofstream fout;
	fout.open(outputFileName);

	fout << nodeNum << endl;
	for (int i = 0; i < nodeNum; ++i) {
		
		fout << newArea[i] << " ";

		for (int j = 0; j < NNB; ++j) {
			
			fout << newNeighbor[i + j * nodeNum] << " ";

			for (int k = 0; k < NDIM; ++k) {
				
				fout << newNormals[i + (j + k * NNB) * nodeNum] << " ";

			}
		}

		fout << endl;
	}

	fout.close();

	// Write info of new data: new output order & partition size
	fout.open(outputInfoName);

	for (int i = 0; i < partNum; ++i)
		fout << vPartSizes[i] << " ";
	fout << endl;

	for (int i = 0; i < nodeNum; ++i)
		fout << vertices[i] << endl;

	fout.close();

	free(newArea);
	free(newNeighbor);
	free(newNormals);
	free(area);
	free(neighbor);
	free(normals);

	// Cleanup vertex partitioning
	free(vPartSizes);
	free(vertices);
	vertices = NULL;
	vPartSizes = NULL;

	return 0;
}























