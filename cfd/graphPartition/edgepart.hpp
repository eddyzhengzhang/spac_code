#ifndef EDGEPART_HPP
#define EDGEPART_HPP

/**
 * @file edgepart.hpp
 * Defines functions used for edge-partitioning the graph with metis.
 */

/**
 * Changes the memory mapping of the input vector.
 * @param edges The list of edges.
 * @param numvertices The original number of vertices.
 * @param numedges The original number of edges.
 * @param numparts The number of partitions to use.
 * @return an array of numparts size, containing the number of edges in each partition
 */
int * edgepart(Edge * edges, int numvertices, int numedges, int numparts);

#endif
