#include <fstream>
#include <iostream>
#include <set>
#include <vector>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

using namespace std;

int main(int argc, char * argv[]) {
	if(argc != 2) {
		cerr << "Expected input: " << argv[0] << "<input file>" << endl;
		exit(0);
	}
	
	ifstream inputFile(argv[1]);
	
	//Get counts for rows, columns, and edges:
	int numrows = 0;
	int numcols = 0;
	int numedges = 0;
	while(true) {
		char c = inputFile.peek();
		if(c == '%') {
			inputFile.ignore(0xffff, '\n');
		}
		else if(c >= '0' && c <= '9') {
			inputFile >> numrows;
			inputFile >> numcols;
			inputFile >> numedges;
			break;
		}
		else {
			char c;
			inputFile >> c;
		}
	}
	
	for(int e = 0; e < numedges; e++) {
		int row;
		int col;
		inputFile >> row;
		inputFile >> col;
		
		int c = inputFile.peek();
		if(c != '\n' && c != EOF) {
			double tmpfloat;
			inputFile >> tmpfloat;
		}
		
		row--;
		col--;
		col += numrows;
		cout << row << " " << col << "\n";
	}
	inputFile.close();
}

