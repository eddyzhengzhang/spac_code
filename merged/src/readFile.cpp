#include <fstream>
#include <iostream>
#include <set>
#include <vector>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "readFile.hpp"

using namespace std;

void readGraph(char * filename, set<int>** & sets, int & numvertices, int & numedges) {
	ifstream inputFile(filename);
	
	int numrows = 0;
	numedges = 0;
	while(inputFile.good()) {
		char c = inputFile.peek();
		if(c == '#') {
			inputFile.ignore(0xffff, '\n');
		}
		else if(c >= '0' && c <= '9') {
			int v1, v2;
			inputFile >> v1;
			inputFile >> v2;
			inputFile.ignore(0xffff, '\n');
			if(v1 + 1 > numrows)
				numrows = v1 + 1;
			if(v2 + 1 > numrows)
				numrows = v2 + 1;
			numedges++;
		}
		else {
			char c;
			inputFile >> c;
		}
	}
	inputFile.clear();
	inputFile.seekg(0, ios::beg);

	//Get total vertex count:
	numvertices = numrows;

	//Initialize adjacency lists:
	sets = (set<int>**) malloc(sizeof(set<int>*) * (numvertices+1));
	for(int x = 1; x <= numvertices; x++) {
		sets[x] = new set<int>;
	}

	//Read in edges:
	while(inputFile.good()) {
		char c = inputFile.peek();
		if(c == '#') {
			inputFile.ignore(0xffff, '\n');
		}
		else if(c >= '0' && c <= '9') {
			int v1, v2;
			inputFile >> v1;
			inputFile >> v2;
			inputFile.ignore(0xffff, '\n');
			sets[v1 + 1]->insert(v2 + 1);
			sets[v2 + 1]->insert(v1 + 1);
		}
		else {
			char c;
			inputFile >> c;
		}
	}
	inputFile.close();
}

void readMatrix(char * filename, set<int>** & sets, int & numvertices, int & numedges, int & numrows, int & numcols) {
	ifstream inputFile(filename);
	
	//Get counts for rows, columns, and edges:
	numrows = 0;
	numcols = 0;
	numedges = 0;
	while(true) {
		char c = inputFile.peek();
		if(c == '%') {
			inputFile.ignore(0xffff, '\n');
		}
		else if(c >= '0' && c <= '9') {
			inputFile >> numrows;
			inputFile >> numcols;
			inputFile >> numedges;
			break;
		}
		else {
			char c;
			inputFile >> c;
		}
	}
	
	//Get total vertex count:
	#ifdef COMBINE_NODES
		numrows = (numrows + CLUSTER_SIZE - 1) / CLUSTER_SIZE;
		numcols = (numcols + CLUSTER_SIZE - 1) / CLUSTER_SIZE;
	#endif
	numvertices = numrows + numcols;
	
	//Initialize adjacency lists:
	sets = (set<int>**) malloc(sizeof(set<int>*) * (numvertices+1));
	for(int x = 1; x <= numvertices; x++) {
		sets[x] = new set<int>;
	}
	
	//Read in edges:
	
	//TODO: deal with comments in the middle of data
	bool w_data_check = false;
	bool w_data = false;
	int real_numedges = 0;
	for(int e = 0; e < numedges; e++) {
		int row;
		int col;
		inputFile >> row;
		inputFile >> col;
		#ifdef COMBINE_NODES
			row = (row + CLUSTER_SIZE - 1) / CLUSTER_SIZE;
			col = (col + CLUSTER_SIZE - 1) / CLUSTER_SIZE;
		#endif
		int c = inputFile.peek();
		if(c != '\n' && c != EOF) {
			if(!w_data_check) {
				w_data_check = true;
				w_data = true;
			} else
				assert(w_data == true);
			double tmpfloat;
			inputFile >> tmpfloat;
		} else {
			if(!w_data_check) {
				w_data_check = true;
				w_data = false;
			} else
				assert(w_data == false);
		}
		col += numrows;
		if(sets[row]->find(col) == sets[row]->end()) {
			assert(sets[col]->find(row) == sets[col]->end());
			real_numedges++;
		} else
			assert(sets[col]->find(row) != sets[col]->end());
		sets[row]->insert(col);
		sets[col]->insert(row);
	}
	inputFile.close();
	cerr << "Matrix includes data: " << w_data << endl;
	#ifdef COMBINE_NODES
		numedges = real_numedges;
	#endif
}
