#include <common.hpp>
#include <edgepart.hpp>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstring>
#include <set>
#include <stdlib.h>
#include <assert.h>
#include <metis.h>
#include <math.h>
using namespace std;

struct node {
	idx_t id, x, y, z, oriID;
	bool ln;
	node * next, * newneighbor;
};

int * edgepart(Edge * edges, int numvertices, int numedges, int numparts) {
	clock_t TTend, TTbegin;
	clock_t Tend, Tbegin;
	double time_spent, time_total;
	Tbegin = clock();
	TTbegin = Tbegin;
	
	//Initialize adjacency lists:
	vector<idx_t> ** sets = (vector<idx_t>**) malloc(sizeof(vector<idx_t>*) * (numvertices));
	for(idx_t x = 0; x < numvertices; x++) {
		sets[x] = new vector<idx_t>;
	}
	//Initialize line Num:
	vector<bool> ** lnum = (vector<bool>**) malloc(sizeof(vector<bool>*) * (numvertices));
	for(idx_t x = 0; x < numvertices; x++) {
		lnum[x] = new vector<bool>;
	}
	
    idx_t real_numedges = 0;
    for(idx_t e = 0; e < numedges; e++) {
    	idx_t row = edges[e].head;
		idx_t col = edges[e].tail;
		
		sets[row]->push_back(col);
		lnum[row]->push_back(true);
		sets[col]->push_back(row);
		lnum[col]->push_back(false);
		real_numedges++;
	}
	
	Tend = clock();
	time_spent = (double)(Tend - Tbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::Load File and Import: %lf\n", time_spent);
	if(numedges != real_numedges) fprintf(stderr, "numedges != real_numedges !!!!\n");
	
	//node * grps = (node*)malloc(sizeof(node)*numvertices+1);	
	Tbegin = clock();

	node * nodes = (node*)calloc(2*real_numedges, sizeof(node));//ARI NOTE TO SELF: changed to calloc to avoid memory errors
	node ** getnode = (node**)calloc(numvertices, sizeof(node*));//ARI NOTE TO SELF: changed to calloc to avoid memory errors
	node ** grps = (node**)calloc(numvertices, sizeof(node*));//ARI NOTE TO SELF: changed to calloc to fix memory error
	idx_t * key = (idx_t*)malloc(sizeof(idx_t)*(numvertices+1));
	

	node * n; //node * root;
	idx_t newid = 0;
	//idx_t i, j, e, idx,c = 0, hashcount = 0, tempindx;
	idx_t i, e, idx,c = 0, hashcount = 0, tempindx;
	bool templn;
	bool fst = true;

	for(idx_t g = 0; g < numvertices; g++) {
		key[g] = c;
		fst = true;
		tempindx = 0;
		if(sets[g]->size() == 0){continue;}
		else{
		for(vector<idx_t>::iterator iter = sets[g]->begin(); iter != sets[g]->end(); ++iter){
			e = *iter;	
			templn= lnum[g]->at(tempindx);
			//fprintf(stderr, "%i", (int)ln);
        	//fprintf(stderr, "%s|", templn ? "true" : "false");

			i = newid; newid++;
			n = &nodes[i];
			n->id = i; 
			n->oriID = g;

			if(fst == false){
				idx = i - 1;
				nodes[idx].y = i;
				nodes[idx].next = n;
				n->x = idx;
				n->y = -1;
				n->z = -1;
				n->next = NULL;
				n->ln = templn;
				hashcount+=2; 
			}
			else{
				n->x = -1;
				n->y = -1;
				n->z = -1;
				n->next = NULL;
				n->ln = templn;
				grps[g] = n;
				getnode[g] = n; 
				fst = false;
			}
			c++;
			tempindx++;
		}
		}
	}
	key[numvertices] = c;
	idx_t single = 0;
	
	c = 0;
	for(idx_t g = 0; g < numvertices; g++){
		if(sets[g]->size() == 0) {
			continue;
		} else{
			fst = true;
			for(vector<idx_t>::iterator iter = sets[g]->begin(); iter != sets[g]->end(); ++iter) {
				e = *iter;
				//if(g > e){
				if(e > g){//ARI NOTE TO SELF: bugfix suggested by Robel
					getnode[e]->z = getnode[g]->id;
					getnode[g]->z = getnode[e]->id;

					getnode[e]->newneighbor = getnode[g];
					getnode[g]->newneighbor = getnode[e];

					hashcount+=2;

					getnode[e] = getnode[e]->next;
					getnode[g] = getnode[g]->next;
				}
				if(g == e) fprintf(stderr, "found loop!\n");
				c++;		
			}
			idx = c - 1;
			n = &nodes[idx];
			if(n->id != grps[g]->id){
				grps[g]->x = n->id;
				n->y = grps[g]->id;
				hashcount+=2;
			} else{
				single+=2;
			}
		}
	}

	idx_t * hash = (idx_t*)malloc(sizeof(idx_t)*(numedges*2+1));
	idx_t * newedges = (idx_t*)malloc(sizeof(idx_t)*hashcount);
	
	c = 0; idx_t ne = 0, ec = 0;
	for(idx_t g = 0; g < numvertices; g++){
		if(sets[g]->size() == 0){continue;}
		else{
		n = grps[g];
		if(n == NULL){fprintf(stderr, "MAJOR ERROR [2]\n"); exit(1);}
		while(n != NULL){
			newedges[ne] = n->z; ne++; ec = 1;
			if(n->x != -1){newedges[ne] = n->x; ne++; ec++;}
			if(n->y != -1){newedges[ne] = n->y; ne++; ec++;}
			hash[n->id] = ec;
			c++; n = n->next;
		}
		}
	}
	hash[c] = ne;
	
	ne = 0; ec = 0;
	for(idx_t e = 0; e <= c; e++){
		ec = hash[e];
		hash[e] = ne;
		ne += ec;
	}
	
	Tend = clock();
	time_spent = (double)(Tend - Tbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::Split: %lf\n", time_spent);
	Tbegin = clock();
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	
	idx_t nVertices = numedges*2;
    idx_t nWeights  = 1;
    int tmp;
    tmp = numparts;
    idx_t nParts = (idx_t)tmp;
    idx_t k = nParts;
    //nParts = (idx_t)tmp;
    //idx_t nEdges    = start;

    idx_t objval;
    idx_t * part = (idx_t*)malloc(sizeof(idx_t)*nVertices);

    //idx_t *adjncy = (idx_t *)&newedges;
    //idx_t *xadj = (idx_t *)&hash;
	
	//idx_t ret = METIS_PartGraphKway(&nVertices,&nWeights, hash, newedges, NULL, NULL, NULL, &nParts, NULL, NULL, NULL, &objval, part);
	METIS_PartGraphKway(&nVertices,&nWeights, hash, newedges, NULL, NULL, NULL, &nParts, NULL, NULL, NULL, &objval, part);
	
	Tend = clock();
	time_spent = (double)(Tend - Tbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::METIS: %lf\n", time_spent);
	Tbegin = clock();
	
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	
	idx_t nodesPerKUpper = (idx_t)ceil((double)numedges/(double)k);
	idx_t nodesPerKLower = (idx_t)floor((double)numedges/(double)k);
	vector<node*> ** finalset = (vector<node*>**) malloc(sizeof(vector<node*>*) * (k));
	for(idx_t x = 0; x < k; x++){finalset[x] = new vector<node*>;} 
	idx_t * NumberofEdgesinK = (idx_t*)malloc(sizeof(idx_t)*(k));
	for(idx_t g = 0; g < k; g++){NumberofEdgesinK[g] = 0;}
	vector<node*> conflict;

	idx_t thisk = 0; 
	idx_t thatk = 0;
	node * neighbor;

	c = 0; ne = 0; ec = 0;
	for(idx_t g = 0; g < numvertices; g++){
		n = grps[g];
		while(n != NULL){
			neighbor = n->newneighbor;
			if(n->ln){
				thisk = part[n->id];
				thatk = part[neighbor->id];
				if(thisk != thatk){
					//conflict.push_back(n);
					if(NumberofEdgesinK[thisk] <= nodesPerKLower){
						finalset[thisk]->push_back(n);
						NumberofEdgesinK[thisk]++;
						ec++;
					//}else if(NumberofEdgesinK[thisk] <= nodesPerKLower){
					}else if(NumberofEdgesinK[thatk] <= nodesPerKLower){//ARI NOTE TO SELF: bugfix suggested by Robel
						finalset[thatk]->push_back(n);
						NumberofEdgesinK[thatk]++;
						ec++;
					}else{
						// Just add it to the fist k 
						finalset[thisk]->push_back(n);
						NumberofEdgesinK[thisk]++;
						ec++;
					}
				}else{
					finalset[thisk]->push_back(n);
					NumberofEdgesinK[thisk]++;
					ec++;
				}			
			}
			c++; n = n->next;
		}
	}
	assert(ec == real_numedges);
	
	for(thisk = 0; thisk < k; thisk++){
		if(NumberofEdgesinK[thisk] < nodesPerKLower){
			for(thatk = 0; thatk < k; thatk++){
				if(NumberofEdgesinK[thatk] > nodesPerKUpper){
					while(NumberofEdgesinK[thisk] < nodesPerKLower && NumberofEdgesinK[thatk] > nodesPerKLower){
						n = finalset[thatk]->back();
    					finalset[thatk]->pop_back();
    					NumberofEdgesinK[thatk]--;
    					finalset[thisk]->push_back(n);
    					NumberofEdgesinK[thisk]++;
					}
				}
				assert(NumberofEdgesinK[thisk] <= nodesPerKUpper);
				if(NumberofEdgesinK[thisk] == nodesPerKLower){break;}
			}
		}
	}
	for(thisk = 0; thisk < k; thisk++){
		if(NumberofEdgesinK[thisk] > nodesPerKUpper){
			for(thatk = 0; thatk < k; thatk++){
				if(NumberofEdgesinK[thatk] < nodesPerKUpper){
					n = finalset[thisk]->back();
					finalset[thisk]->pop_back();
					NumberofEdgesinK[thisk]--;
					finalset[thatk]->push_back(n);
					NumberofEdgesinK[thatk]++;
				}
				assert(NumberofEdgesinK[thisk] >= nodesPerKLower);
				if(NumberofEdgesinK[thisk] == nodesPerKUpper){break;}
			}
		}
	}
	
	for(thisk = 0; thisk < k; thisk++){
		if(NumberofEdgesinK[thisk] > nodesPerKUpper || NumberofEdgesinK[thisk] < nodesPerKLower){
			fprintf(stderr, "Balancing ERROR at [k = %d]", thisk);
		}
	}
	
	Tend = clock();
	time_spent = (double)(Tend - Tbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::repartition: %lf\n", time_spent);
	
    TTend = clock();
	time_total = (double)(TTend - TTbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::Total: %lf\n", time_total);
	
	//Allocate space for return value:
	int * partsize = (int *) malloc(sizeof(int) * k);
	int totalpartsize = 0;
	
	i = 0;
	for(int thisk = 0; thisk < k; thisk++) {
		partsize[thisk] = finalset[thisk]->size();
		totalpartsize += partsize[thisk];
		
        for(vector<node*>::iterator it = finalset[thisk]->begin(); it != finalset[thisk]->end(); it++) {
        	n = *it; 
        	neighbor = n->newneighbor;
        	//fprintf(stderr, "%s|", n->ln ? "true" : "false");
        	if(n->ln) {
        		edges[i].tail = (int)neighbor->oriID;
				edges[i].head = (int)n->oriID;
				i++;
			}
        }
    }
	
	assert(i == real_numedges);
	
	if(totalpartsize != numedges) {
		cerr << "ERROR ~508: mismatched edge count.\n";
	}
	
	//Cleanup
	for(int x = 0; x < numvertices; x++) {
		delete sets[x];
		delete lnum[x];
	}
	free(sets);
	free(lnum);
	for(int x = 0; x < k; x++) {
		delete finalset[x];
	}
	free(finalset);
	free(NumberofEdgesinK);
	free(hash);
	free(newedges);
	free(part);
	free(key);
	
	//More cleanup
	free(nodes);
	free(getnode);
	free(grps);
	
	fflush(stderr);
	fflush(stdout);
	return partsize;
}
