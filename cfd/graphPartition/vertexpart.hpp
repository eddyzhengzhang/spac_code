#ifndef VERTEXPART_HPP
#define VERTEXPART_HPP

/**
 * @file vertexpart.hpp
 * Defines functions used for vertex-partitioning the graph with metis.
 */

/**
 * Changes the memory mapping of the input vector.
 * @param edges The list of edges.
 * @param numvertices The original number of vertices.
 * @param numedges The original number of edges.
 * @param numparts The number of partitions to use.
 * @param vertices Byref parameter used to return the list of vertices such that each partition is contiguous.
 * @return an array of numparts size, containing the number of vertices in each partition
 */
int * vertexpart(Edge * edges, int numvertices, int numedges, int numparts, int *& vertices);

#endif
