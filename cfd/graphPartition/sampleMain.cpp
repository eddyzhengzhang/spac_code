#include <iostream>
#include <stdlib.h>
#include <common.hpp>
#include <edgepart.hpp>
#include <vertexpart.hpp>
using namespace std;

using namespace std;

int main (int argc, char **argv) {
	//Generate a small & simple graph
	int numedges = 10;
	Edge * edges = (Edge *) malloc(sizeof(Edge) * numedges);
	for(int x = 0; x < numedges; x++) {
		edges[x].head = x;
		edges[x].tail = x + 1;
	}
	int numvertices = edges[numedges - 1].tail + 1;
	
	//Do vertex partitioning:
	int numparts = 3;
	int * vertices;
	int * vPartSizes = vertexpart(edges, numvertices, numedges, numparts, vertices);
	
	//Print vertex partitioning:
	cout << "Vertex partitions:\n";
	int sizesum = 0;
	for(int part = 0; part < numparts; part++) {
		cout << "{";
		for(int offset = 0; offset < vPartSizes[part]; offset++) {
			if(offset) {
				cout << ", ";
			}
			cout << vertices[sizesum + offset];
		}
		sizesum += vPartSizes[part];
		cout << "}\n";
	}
	
	//Cleanup vertex partitioning:
	free(vPartSizes);
	free(vertices);
	vertices = NULL;
	vPartSizes = NULL;
	
	//Do edge partitioning:
	numparts = 3;
	int * ePartSizes = edgepart(edges, numvertices, numedges, numparts);
	
	//Print edge partitioning:
	cout << "Edge partitions:\n";
	sizesum = 0;
	for(int part = 0; part < numparts; part++) {
		cout << "{";
		for(int offset = 0; offset < ePartSizes[part]; offset++) {
			if(offset) {
				cout << ", ";
			}
			cout << "(";
			cout << edges[sizesum + offset].head;
			cout << ",";
			cout << edges[sizesum + offset].tail;
			cout << ")";
		}
		sizesum += ePartSizes[part];
		cout << "}\n";
	}
	
	//Cleanup graph data:
	free(edges);
	edges = NULL;
	
	cout.flush();
	cerr << "Program terminated without crashing.\n";
	return 0;

}
