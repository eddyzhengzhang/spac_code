#ifndef READFILE_HPP
#define READFILE_HPP

#include <set>
using namespace std;

void readGraph(char * filename, set<int>** & sets, int & numvertices, int & numedges);

void readMatrix(char * filename, set<int>** & sets, int & numvertices, int & numedges, int & numrows, int & numcols);

#endif
