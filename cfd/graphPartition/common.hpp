#ifndef COMMON_HPP
#define COMMON_HPP

/**
 * @file common.hpp
 * Defines values important to multiple functions.
 */

struct Edge {
	int head;
	int tail;
};

//This hopefully lets the set class handle Edge pointers properly:
struct EdgeComp
{
	bool operator()(const Edge* lhs, const Edge* rhs) const {
		return lhs->head < rhs->head || (lhs->head == rhs->head && lhs->tail < rhs->tail);
	}
};

#endif
