#ifndef REPARTITION_HPP
#define REPARTITION_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <vector>
#include <set>
#include <stdlib.h>
#include <assert.h>

using namespace std;

class vertex{
  public:
    int id;
    int pid;

    int oedge_id;
    int ori_id;
};

class edge{
  public:
    int begin;
    int end;
    bool is_ori;
    bool is_cut;
    int partition1; // The first partition it belongs
    int partition2; // The second partition it belongs

    int part_id;
};

class graph{
  public:
    int vertex_num;
    int edge_num;
    vertex * vertexes;
    edge * edges;
};

class partition_score{
  public:
    int id;
    int score;
};

class partition{
  public:
    int id;
    int vertex_num;
    int edge_num;
    vector<vertex> vlist;

    // transformed partition
    int ori_vertex_num;
    vector<int> ovlist;
    int ori_edge_num;
    vector<int> oelist;
    vector<partition_score> waitinglist;
};

class original_vertex{
  public:
    int begin;
    int end;
    vector<partition_score> plist;

    int part_id;
};

void repartition(int numvertices, int numedges, int numnewvertices, int numnewedges, int numparts, vector<int> ** newgraph, int * partitions, int * mapping);

#endif
