========
BUILDING
========

This directory is read-only, so you should make your own copy. Note that the 
code makes use of the metis library, which is located in my own home directory, 
and is also read-only. You can make your own copy of metis and change the paths 
in the Makefile if you want to, but this should not be necessary.

To build the object files, as well as a simple main function I included, simply 
run the 'make' command inside your copy of this directory.

By default, the Makefile uses the -O3 flag to enable optimization. If you want  
to compile with debugging info, then use the command 'make debug' instead. This 
will replace the -O3 flag with the -O0 and -g flags.

===================
SOURCE CODE - FILES
===================

edgepart.cpp, edgepart.hpp
This contains the edge partition function, edgepart.

vertexpart.cpp, vertexpart.hpp
This contains the vertex partition function, vertexpart.

common.hpp
This defines the Edge struct, which is used in the other files.

sampleMain.cpp
This contains a main function which generates a small graph with hard-coded values,
and runs both partitioning functions to split it into three partitions, outputting 
the results to the terminal via stdout.

=======================================
SOURCE CODE - FUNCTION INPUT AND OUTPUT
=======================================

int * edgepart(Edge * edges, int numvertices, int numedges, int numparts);

The inputs to edgepart function are as follows.
 First, edges is the list of edges. 
  The Edge struct contains two int values: head and tail, each of which are a vertex.
 Second, numvertices is the number of vertices in the graph.
  Note that if there are n vertices, then the first should be at least 0, and the 
  last should be no more than (n - 1).
 Third, numedges is the size of the 'edges' array.
 Fourth, numparts is the number of partitions to split the edges into.
  This should be at least 2, and no more than numedges.
  
The output from edgepart is handled as follows:
 This function returns an int array of size 'numparts'. The nth value in this array 
 is the size of the nth partition.
 Additionally, this function reorders the 'edges' list so that partitions are sorted 
 and contiguous. For example, If the first partition is of size x, then the first x 
 edges correspond to that partition; if the second partition is of size y, then the 
 next y edges correspond to the second partition.

int * vertexpart(Edge * edges, int numvertices, int numedges, int numparts, int *& vertices);

The inputs to vertexpart are as follows:
 First, edges is the list of edges. 
  The Edge struct contains two int values: head and tail, each of which are a vertex.
 Second, numvertices is the number of vertices in the graph.
  Note that if there are n vertices, then the first should be at least 0, and the 
  last should be no more than (n - 1).
 Third, numedges is the size of the 'edges' array.
 Fourth, numparts is the number of partitions to split the vertices into.
  This should be at least 2, and no more than numvertices.

The output from vertexpart is handled as follows:
 This function returns an int array of size 'numparts'. The nth value in this array 
 is the size of the nth partition.
 The fifth parameter, 'vertices' is set to a list of vertices sorted by partition.
 For example, if the first partition is of size x, then the first x vertices in the list
 correspond to the first partition; if the second partition is of size y, then the next 
 y vertices correspond to the second partition.
