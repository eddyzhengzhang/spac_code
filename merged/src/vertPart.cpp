#include <vector>
#include <iostream>
#include <fstream>
#include <cstring>
#include <set>
#include <stdlib.h>
#include <assert.h>
#include <metis.h>
#include <math.h>
#include <string>

#include "vertPart.hpp"

using namespace std;

int * vertexpart(Edge * edges, int numvertices, int numedges, int numparts) {
	clock_t Tend, Tbegin;
	double time_spent;
	Tbegin = clock();
	
	//Allocate main metis inputs:
	idx_t * xadj = (idx_t *) malloc((numvertices + 1) * sizeof(idx_t));
	idx_t * adjncy = (idx_t *) malloc(2 * numedges * sizeof(idx_t));
	
	//Generate edge set to make it easier to count, avoiding issues where edge list might contain both (x,y) and (y,x):
	set<Edge*, EdgeComp> edgeSet;
	Edge * edgeSetMem = (Edge *) malloc(numedges * sizeof(Edge));//so I don't have waste memops allocating every Edge separately
	for(int edge = 0; edge < numedges; edge++) {
		int x = edges[edge].head;
		int y = edges[edge].tail;
		if(x > y) {
			int temp = x;
			x = y;
			y = temp;
		}
		edgeSetMem[edge].head = x;
		edgeSetMem[edge].tail = y;
		edgeSet.insert(&edgeSetMem[edge]);
	}
	
	//Count edges per vertex in linear time:
	int * vertexCounters = (int *) calloc(numvertices, sizeof(int));
	std::set<Edge*>::iterator it;
	for(it = edgeSet.begin(); it != edgeSet.end(); it++) {
		Edge * e = *it;
		int head = e->head;
		int tail = e->tail;
		vertexCounters[head]++;
		if(head != tail) {
			vertexCounters[tail]++;
		}
	}
	
	//Fill in xadj list in linear time:
	int tempsum = 0;
	for(int vertex = 0; vertex < numvertices; vertex++) {
		xadj[vertex] = tempsum;
		tempsum += vertexCounters[vertex];
	}
	xadj[numvertices] = tempsum;
	
	//Fill in adjncy list in linear time:
	memset(vertexCounters, 0, numvertices * sizeof(int));
	for(it = edgeSet.begin(); it != edgeSet.end(); it++) {
		Edge * e = *it;
		int head = e->head;
		int tail = e->tail;
		
		int loc = vertexCounters[head] + xadj[head];
		adjncy[loc] = tail;
		vertexCounters[head]++;
		
		if(head != tail) {
			loc = vertexCounters[tail] + xadj[tail];
			adjncy[loc] = head;
			vertexCounters[tail]++;
		}
	}
	
	/*
	for(int x = 0; x < numvertices + 1; x++) {
		cerr << xadj[x] << ", ";
	}
	cerr << "\n";
	for(int x = 0; x < numedges; x++) {
		cerr << adjncy[x] << ", ";
	}
	cerr << "\n";
	*/
	
	//Free variables we no longer need:
	edgeSet.clear();
	free(edgeSetMem);
	free(vertexCounters);
	
	Tend = clock();
	time_spent = (double)(Tend - Tbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::Prepare input for metis: %lf\n", time_spent);
	
	Tbegin = clock();
	
	//Some more parameters for metis function
	idx_t nVertices = numvertices;
    idx_t nWeights  = 1;
    idx_t nParts = numparts;
    idx_t objval;
    idx_t * part = (idx_t*)malloc(sizeof(idx_t)*nVertices);
	
	//Run metis:
	idx_t ret = METIS_PartGraphKway(&nVertices,&nWeights, xadj, adjncy, NULL, NULL, NULL, &nParts, NULL, NULL, NULL, &objval, part);
	if(ret != METIS_OK) {
		cerr << "ERROR: metis threw an error.\n";
	}
	
	Tend = clock();
	time_spent = (double)(Tend - Tbegin) / CLOCKS_PER_SEC;
	fprintf(stderr, "Time::metis: %lf\n", time_spent);
	
	//Free metis inputs that we no longer need:
	free(xadj);
	free(adjncy);
	
	/*
	//print output:
	cout << "vertexpart's return value is an int array with " << nVertices << " values:\n";
	for(int x = 0; x < nVertices; x++) {
		cout << part[x] << "\n";
	}
	cout << "\n";
	*/
	
	return part;
}
