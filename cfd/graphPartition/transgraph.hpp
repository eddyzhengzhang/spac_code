#ifndef TRANSGRAPH_HPP
#define TRANSGRAPH_HPP

/**
 * @file transgraph.hpp
 * Defines functions used for edge-partitioning the matrix with metis
 */

/**
 * Changes the memory mapping of the input vector.
 * @param row_indices The list of row indices for nonzero values.
 * @param col_indices The list of column indices for nonzero values.
 * @param data The list of nonzero values.
 * @param vec The input vector.
 * @param numrows The size of the output vector.
 * @param numcols The size of the input vector.
 * @param numtasks The number of nonzero matrix elements.
 * @param numparts The number of partitions to use.
 * @param singleNode Should be true iff row 'x' is the same node as column 'x'.
 * @return an array of numparts size, containing the number of edges in each partition
 */
int * transgraph(int * row_indices, int * col_indices, DATATYPE * data, DATATYPE * vec, int numrows, int numcols, int numtasks, int numparts, bool singleNode);

#endif
