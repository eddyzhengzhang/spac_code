#include <fstream>
#include <iostream>
#include <set>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include <string.h>

//#define MATRIX_W_DATA

#define USE_DEGREE_THRESHOLD
#define DEGREE_THRESHOLD 16

using namespace std;

int main(int argc, char * argv[]) {
	if(argc != 2) {
		cerr << "Expected input: " << argv[0] << " <input file>" << endl;
		exit(0);
	}
	
	//Figure out file names:
	ostringstream temp;
	temp << argv[1];
	temp << ".old.graph";
	char * oldfileName = strdup(temp.str().c_str());
	ostringstream temp2;
	temp2 << argv[1];
	temp2 << ".new.graph";
	char * newfileName = strdup(temp2.str().c_str());
	ostringstream temp3;
	temp3 << argv[1];
	temp3 << ".map";
	char * mapfileName = strdup(temp3.str().c_str());
	
	//Prepare input file:
	ifstream inputFile(argv[1]);
#ifndef GMODE
	//Get counts for rows, columns, and edges:
		int numrows = 0;
		int numcols = 0;
		int numedges = 0;
		while(true) {
			char c = inputFile.peek();
			if(c == '%') {
				inputFile.ignore(0xffff, '\n');
			}
			else if(c >= '0' && c <= '9') {
				inputFile >> numrows;
				inputFile >> numcols;
				inputFile >> numedges;
				break;
			}
			else {
				char c;
				inputFile >> c;
			}
		}
	
		//Get total vertex count:
		int numvertices = numrows + numcols;
	
		//Initialize adjacency lists:
		set<int> ** sets = (set<int>**) malloc(sizeof(set<int>*) * (numvertices+1));
		for(int x = 1; x <= numvertices; x++) {
			sets[x] = new set<int>;
		}
	
		//Read in edges:
		//TODO: deal with comments in the middle of data
		for(int e = 0; e < numedges; e++) {
			int row;
			int col;
			inputFile >> row;
			inputFile >> col;
#ifdef MATRIX_W_DATA
            double tmpfloat;
			inputFile >> tmpfloat;
#endif
			col += numrows;
			sets[row]->insert(col);
			sets[col]->insert(row);
		}
		inputFile.close();
#else
		int numrows = 0;
		int numedges = 0;
		while(true) {
			char c = inputFile.peek();
			if(c == '%') {
				inputFile.ignore(0xffff, '\n');
			}
			else if(c >= '0' && c <= '9') {
				inputFile >> numrows;
				inputFile >> numedges;
				inputFile.ignore(0xffff, '\n');
				break;
			}
			else {
				char c;
				inputFile >> c;
			}
		}
	
		//Get total vertex count:
		int numvertices = numrows;
	
		//Initialize adjacency lists:
		set<int> ** sets = (set<int>**) malloc(sizeof(set<int>*) * (numvertices+1));
		for(int x = 1; x <= numvertices; x++) {
			sets[x] = new set<int>;
		}
	
		//Read in edges:
		//TODO: deal with comments in the middle of data
		int edgecount = 0;
		string line;
		for(int e = 1; e <= numvertices; e++) {
			string line;
			getline(inputFile, line);
			stringstream ss(line);
			int row;
			while(ss >> row) {
				edgecount++;
				sets[e]->insert(row);
				row[sets]->insert(e);
			}
		}
		inputFile.close();
		
		edgecount = edgecount >> 1;
		if(edgecount != numedges) {
			cerr << "SANITY CHECK ERROR ~120: incorrect edge count (" << edgecount << "/" << numedges << ").\n";
		}
#endif
	
	//Output old graph:
	ofstream oldfile(oldfileName);
	oldfile << numvertices << " " << numedges << endl;
	for(int v = 1; v <= numvertices; v++) {
		oldfile << " ";
		for(set<int>::iterator iter = sets[v]->begin(); iter != sets[v]->end(); ++iter) {
			oldfile << *iter;
			oldfile << " ";
		}
		oldfile << endl;
	}
	oldfile.close();
	
	//Calculate mapping:
	int counter = 1;
	int * mapping = (int*) malloc(sizeof(int) * (numvertices+2));
	for(int x = 1; x <= numvertices; x++) {
		mapping[x] = counter;
		int neighbors = sets[x]->size();
		if(neighbors > 0) {
			counter += neighbors;
		}
	}
	mapping[numvertices+1] = counter;
	
	//Write mapping to file:
	ofstream mapfile(mapfileName);
	mapfile << numvertices << endl;
	for(int v = 1; v <= numvertices + 1; v++) {
		mapfile << mapping[v];
		mapfile << endl;
	}
	mapfile.close();
	
	//Get new vertex count:
	int numnewvertices = numedges * 2;
	
	//Initialize new sets:
	vector<int> ** newsets = (vector<int>**) malloc(sizeof(vector<int>*) * (numnewvertices+1));
	for(int x = 1; x <= numnewvertices; x++) {
		newsets[x] = new vector<int>;
	}
	
	//Add old edges to new graph:
	int numnewedges = 0;
	for(int x = 1; x <= numvertices; x++) {
		int self = mapping[x];
		for(set<int>::iterator iter = sets[x]->begin(); iter != sets[x]->end(); ++iter) {
			int oldneighbor = *iter;
			if(oldneighbor > x) {
				//Note to self; each vertex has at most ONE old edge
				int neighbor = mapping[oldneighbor];
				while(newsets[neighbor]->size() > 0) {
					neighbor++;
				}
				while(newsets[self]->size() > 0) {
					self++;
				}
				
				if(neighbor >= mapping[oldneighbor + 1]) {
					cerr << "SANITY CHECK ERROR ~132: no valid vertex.\n";
				}
				if(self >= mapping[x + 1]) {
					cerr << "SANITY CHECK ERROR ~138: no valid vertex.\n";
				}
				
				newsets[neighbor]->push_back(self);
				newsets[self]->push_back(neighbor);
				numnewedges++;
			}
		}
	}
	
	//Error check:
	for(int x = 1; x <= numnewvertices; x++) {
		if(newsets[x]->size() != 1) {
			cerr << "SANITY CHECK ERROR ~150: node has " << newsets[x]->size() << " new edges.\n";
		}
	}
	
	//Connect 'rings' in new graph:
	for(int x = 1; x <= numvertices; x++) {
		if(mapping[x+1] == mapping[x]) {
			continue;
		}
		
#ifdef USE_DEGREE_THRESHOLD
        if(mapping[x+1] - mapping[x] < DEGREE_THRESHOLD)
        {
#endif
		for(int y = mapping[x]; y < mapping[x+1] - 1; y++) {
			newsets[y]->push_back(y + 1);
			newsets[y + 1]->push_back(y);
			numnewedges++;
		}
		
		if(mapping[x+1] > mapping[x] + 2) {
			newsets[mapping[x]]->push_back(mapping[x+1]-1);
			newsets[mapping[x+1]-1]->push_back(mapping[x]);
			numnewedges++;
		}
#ifdef USE_DEGREE_THRESHOLD
        }
#endif
	}
	
	//Output new graph:
	ofstream newfile(newfileName);
	newfile << numnewvertices << " " << numnewedges << endl;
	for(int v = 1; v <= numnewvertices; v++) {
		newfile << " ";
		for(vector<int>::iterator iter = newsets[v]->begin(); iter != newsets[v]->end(); ++iter) {
			newfile << *iter;
			newfile << " ";
		}
		newfile << endl;
	}
	newfile.close();
	
	free(mapping);
	free(sets);
	free(newsets);
}

