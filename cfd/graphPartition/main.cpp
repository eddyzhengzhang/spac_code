#include "transgraph.hpp"
#include "common.hpp"
#include <fstream>
#include <iostream>
#include <cstdio>

using namespace std;

int main(int argc, char* argv[]) {

	FILE *fp, *fp_vec, *fp_out;
	char line[1024]; 
	int *row_indices, *col_indices;
	float *data, *b, *t;
	int i;
	int n; // number of nonzero elements in data
	int nr; // number of rows in matrix
	int nc; // number of columns in matrix

    // Open input file and read to end of comments
    if (argc != 4) {
      printf("Usage: %s inputMatFileName inputVecFileName outputFileName\n", argv[0]);
      abort(); 
    }

    //Open mat file
    if ((fp = fopen(argv[1], "r")) == NULL) {
      printf("Unable to open input matrix file\n");
      abort();
    }

    //Open vec file
    if ((fp_vec = fopen(argv[2], "r")) == NULL) {
    	printf("Unable to open input vector file\n");
    	abort();
    }
    
    //Open output vec file
    if ((fp_out = fopen(argv[3], "wt")) == NULL) {
    	printf("Unable to write to output vector file\n");
    	abort();
    }

	// READ first non-comment line in the matrix file
	fgets(line, 128, fp);
	while (line[0] == '%') {
		fgets(line, 128, fp); 
	}

  // Read number of rows (nr), number of columns (nc) and
  // number of elements and allocate memory for row_indices, col_indices, data, b and t.
  sscanf(line,"%d %d %d\n", &nr, &nc, &n);
  row_indices = (int *) calloc(n, sizeof(int));
  col_indices = (int *) malloc(n*sizeof(int));
  data = (float *) malloc(n*sizeof(float));
  b = (float *) malloc(nc*sizeof(float));
  t = (float *) calloc(nr, sizeof(float));

  // Read data in coordinate format and initialize sparse matrix


	for (i=0; i<n; i++) {
		fscanf(fp,"%d %d", &(row_indices[i]),  &(col_indices[i]));
		if(fgetc(fp) != '\n'){
			fscanf(fp,"%f\n", &(data[i]));
//			data[i] = 1.0;
		}
		else{
			data[i] = 1.0;
		}
		col_indices[i]--;  // start numbering at 0
		row_indices[i]--;
	}

	int parts = 2;
	int *res = (int *) calloc(parts, sizeof(int));

	res = transgraph(row_indices, col_indices, data, b, nr, nc, n, parts, false);

	for (int i = 0; i < parts; ++i)
		cout << res[i] << endl; 
	
	fclose(fp);
	fclose(fp_vec);
	fclose(fp_out);

	return 0;
}
