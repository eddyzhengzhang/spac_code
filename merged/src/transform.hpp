#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include <set>
#include <vector>
using namespace std;

void transform(set<int>** sets, int numvertices, int numedges, vector<int>** & newsets, int* & mapping, int & numnewvertices, int & numnewedges);

#endif
