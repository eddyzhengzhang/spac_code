#include <fstream>
#include <iostream>
#include <set>
#include <vector>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

using namespace std;

//#define ADA_DEGREE_THRESHOLD
//#define USE_DEGREE_THRESHOLD
//#define DEGREE_THRESHOLD 16
#define DEGREE_THRESHOLD (4 * max((numedges/numrows), (numedges/numcols)))

//#define GMODE
#define EMODE

//#define COMBINE_NODES
//#define CLUSTER_SIZE 4

//#define OUTPUT_EDGE

int main(int argc, char * argv[]) {
	if(argc != 2) {
		cerr << "Expected input: " << argv[0] << " <input file>" << endl;
		exit(0);
	}
	
	//Figure out file names:
	ostringstream temp;
	temp << argv[1];
	temp << ".old.graph";
	char * oldfileName = strdup(temp.str().c_str());
	ostringstream temp2;
	temp2 << argv[1];
	temp2 << ".new.graph";
	char * newfileName = strdup(temp2.str().c_str());
	ostringstream temp3;
	temp3 << argv[1];
	temp3 << ".map";
	char * mapfileName = strdup(temp3.str().c_str());
	ostringstream temp4;
	temp4 << argv[1];
	temp4 << ".newweight.graph";
	char * weightfileName = strdup(temp4.str().c_str());
	ostringstream temp6;
	temp6 << argv[1];
	temp6 << ".vweight.graph";
	char * vweightfileName = strdup(temp6.str().c_str());
#ifdef OUTPUT_EDGE
	ostringstream temp5;
	temp5 << argv[1];
	temp5 << ".newedge.graph";
	char * edgefileName = strdup(temp5.str().c_str());
#endif
	
	//Prepare input file:
	ifstream inputFile(argv[1]);
#if defined (GMODE)
	int numrows = 0;
	int numedges = 0;
	while(true) {
		char c = inputFile.peek();
		if(c == '%') {
			inputFile.ignore(0xffff, '\n');
		}
		else if(c >= '0' && c <= '9') {
			inputFile >> numrows;
			inputFile >> numedges;
			inputFile.ignore(0xffff, '\n');
			break;
		}
		else {
			char c;
			inputFile >> c;
		}
	}

	//Get total vertex count:
	int numvertices = numrows;

	//Initialize adjacency lists:
	set<int> ** sets = (set<int>**) malloc(sizeof(set<int>*) * (numvertices+1));
	for(int x = 1; x <= numvertices; x++) {
		sets[x] = new set<int>;
	}

	//Read in edges:
	//TODO: deal with comments in the middle of data
	int edgecount = 0;
	string line;
	for(int e = 1; e <= numvertices; e++) {
		string line;
		getline(inputFile, line);
		stringstream ss(line);
		int row;
		while(ss >> row) {
			edgecount++;
			sets[e]->insert(row);
			sets[row]->insert(e);
		}
	}
	inputFile.close();
	
	edgecount = edgecount >> 1;
	if(edgecount != numedges) {
		cerr << "SANITY CHECK ERROR ~120: incorrect edge count (" << edgecount << "/" << numedges << ").\n";
	}
#elif defined (EMODE)
	int numrows = 0;
	int numedges = 0;
	while(inputFile.good()) {
		char c = inputFile.peek();
		if(c == '#') {
			inputFile.ignore(0xffff, '\n');
		}
		else if(c >= '0' && c <= '9') {
            int v1, v2;
			inputFile >> v1;
			inputFile >> v2;
			inputFile.ignore(0xffff, '\n');
            if(v1 + 1 > numrows)
                numrows = v1 + 1;
            if(v2 + 1 > numrows)
                numrows = v2 + 1;
            numedges++;
		}
		else {
			char c;
			inputFile >> c;
		}
	}
    inputFile.clear();
    inputFile.seekg(0, ios::beg);

	//Get total vertex count:
	int numvertices = numrows;

	//Initialize adjacency lists:
	set<int> ** sets = (set<int>**) malloc(sizeof(set<int>*) * (numvertices+1));
	for(int x = 1; x <= numvertices; x++) {
		sets[x] = new set<int>;
	}

	//Read in edges:
	ostringstream temp7;
	temp7 << argv[1];
	temp7 << ".dat";
	char * datfileName = strdup(temp7.str().c_str());
	ofstream datfile(datfileName);
	//TODO: deal with comments in the middle of data
	int edgecount = 0;
	while(inputFile.good()) {
		char c = inputFile.peek();
		if(c == '#') {
			inputFile.ignore(0xffff, '\n');
		}
		else if(c >= '0' && c <= '9') {
            int v1, v2;
			inputFile >> v1;
			inputFile >> v2;
			inputFile.ignore(0xffff, '\n');
			sets[v1 + 1]->insert(v2 + 1);
			sets[v2 + 1]->insert(v1 + 1);
            datfile << v1 + 1 << " " << v2 + 1 << endl;
            datfile << v2 + 1 << " " << v1 + 1 << endl;
		}
		else {
			char c;
			inputFile >> c;
		}
	}
	inputFile.close();
    datfile.close();
#else
	//Get counts for rows, columns, and edges:
	int numrows = 0;
	int numcols = 0;
	int numedges = 0;
	while(true) {
		char c = inputFile.peek();
		if(c == '%') {
			inputFile.ignore(0xffff, '\n');
		}
		else if(c >= '0' && c <= '9') {
			inputFile >> numrows;
			inputFile >> numcols;
			inputFile >> numedges;
			break;
		}
		else {
			char c;
			inputFile >> c;
		}
	}

	//Get total vertex count:
#ifdef COMBINE_NODES
    numrows = (numrows + CLUSTER_SIZE - 1) / CLUSTER_SIZE;
    numcols = (numcols + CLUSTER_SIZE - 1) / CLUSTER_SIZE;
#endif
	int numvertices = numrows + numcols;

	//Initialize adjacency lists:
	set<int> ** sets = (set<int>**) malloc(sizeof(set<int>*) * (numvertices+1));
	for(int x = 1; x <= numvertices; x++) {
		sets[x] = new set<int>;
	}

	//Read in edges:
#ifdef OUTPUT_EDGE
	set<int> * edgesets = new set<int>[numvertices+1];
#endif

	//TODO: deal with comments in the middle of data
    bool w_data_check = false;
    bool w_data = false;
    int real_numedges = 0;
	for(int e = 0; e < numedges; e++) {
		int row;
		int col;
		inputFile >> row;
		inputFile >> col;
#ifdef COMBINE_NODES
        row = (row + CLUSTER_SIZE - 1) / CLUSTER_SIZE;
        col = (col + CLUSTER_SIZE - 1) / CLUSTER_SIZE;
#endif
        int c = inputFile.peek();
        if(c != '\n' && c != EOF) {
            if(!w_data_check) {
                w_data_check = true;
                w_data = true;
            } else
                assert(w_data == true);
            double tmpfloat;
		    inputFile >> tmpfloat;
        } else {
            if(!w_data_check) {
                w_data_check = true;
                w_data = false;
            } else
                assert(w_data == false);
        }
		col += numrows;
        if(sets[row]->find(col) == sets[row]->end()) {
            assert(sets[col]->find(row) == sets[col]->end());
            real_numedges++;
        } else
            assert(sets[col]->find(row) != sets[col]->end());
		sets[row]->insert(col);
		sets[col]->insert(row);
#ifdef OUTPUT_EDGE
        edgesets[row].insert(e);
        edgesets[col].insert(e);
#endif
	}
	inputFile.close();
    cerr << "Matrix includes data: " << w_data << endl;
#ifdef COMBINE_NODES
    numedges = real_numedges;
#endif

#endif

#ifdef OUTPUT_EDGE
    cerr << "Output edge based graph" << endl;
	vector<int> * newedgesets = new vector<int>[numedges];
    int numedgesedges = 0;

    // go through each vertex to connect edges that share them
	for(int v = 1; v <= numvertices; v++) {
        if(edgesets[v].size() < 2)
            continue;
        set<int>::iterator startp = edgesets[v].begin();
        set<int>::iterator endp = edgesets[v].begin();
        endp++;
        while(endp != edgesets[v].end()) {
            //assert(newedgesets[*startp].find(*endp) == newedgesets[*startp].end());
            //assert(newedgesets[*endp].find(*startp) == newedgesets[*endp].end());
            assert(find(newedgesets[*startp].begin(), newedgesets[*startp].end(), *endp) == newedgesets[*startp].end());
            assert(find(newedgesets[*endp].begin(), newedgesets[*endp].end(), *startp) == newedgesets[*endp].end());
            newedgesets[*startp].push_back(*endp);
            newedgesets[*endp].push_back(*startp);
            numedgesedges++;
            startp++;
            assert(startp == endp);
            endp++;
        }
    }

	ofstream edgefile(edgefileName);
    edgefile << numedges << " " << numedgesedges << endl;
    for(int e = 0; e < numedges; e++) {
		for(vector<int>::iterator it = newedgesets[e].begin(); it != newedgesets[e].end(); ++it) {
            edgefile << *it + 1 << " ";
        }
        edgefile << endl;
    }
    edgefile.close();
    return 0;
#endif
	
	//Output old graph:
    int max_degree = 0;
	ofstream oldfile(oldfileName);
	ofstream vweightfile(vweightfileName);
	oldfile << numvertices << " " << numedges << endl;
	vweightfile << numvertices << " " << numedges << " 010" << endl;
	for(int v = 1; v <= numvertices; v++) {
		oldfile << " ";
		vweightfile << sets[v]->size() << " ";
        if(sets[v]->size() > max_degree)
            max_degree = sets[v]->size();
		for(set<int>::iterator iter = sets[v]->begin(); iter != sets[v]->end(); ++iter) {
			oldfile << *iter;
			oldfile << " ";
			vweightfile << *iter;
			vweightfile << " ";
		}
		oldfile << endl;
        vweightfile << endl;
	}
	oldfile.close();
	vweightfile.close();

    // identify graph feature
    // produce degree histogram
    int * histogram = new int[max_degree+1];
    for(int i = 0; i < max_degree+1; i++)
        histogram[i] = 0;
    for(int i = 0; i < numvertices; i++)
    {
        assert(sets[i+1]->size() <= max_degree);
        histogram[sets[i+1]->size()]++;
    }
    int sum_now = 0;
    int degree_threshold = -1;
    for(int i = 0; i < max_degree+1; i++)
    {
        sum_now += histogram[i];
        if( (double)sum_now / (double)numvertices >= 0.998 ) {
            degree_threshold = i;
            break;
        }
    }
    if(degree_threshold == -1){
        assert(sum_now == numvertices);
        degree_threshold = max_degree;
    }
	
	//Calculate mapping:
	int counter = 1;
	int * mapping = (int*) malloc(sizeof(int) * (numvertices+2));
	for(int x = 1; x <= numvertices; x++) {
		mapping[x] = counter;
		int neighbors = sets[x]->size();
		if(neighbors > 0) {
			counter += neighbors;
		}
	}
	mapping[numvertices+1] = counter;
	
	//Write mapping to file:
	ofstream mapfile(mapfileName);
	mapfile << numvertices << endl;
	for(int v = 1; v <= numvertices + 1; v++) {
		mapfile << mapping[v];
		mapfile << endl;
	}
	mapfile.close();
	
	//Get new vertex count:
	int numnewvertices = numedges * 2;
	
	//Initialize new sets:
	vector<int> ** newsets = (vector<int>**) malloc(sizeof(vector<int>*) * (numnewvertices+1));
	for(int x = 1; x <= numnewvertices; x++) {
		newsets[x] = new vector<int>;
	}
	
	//Add old edges to new graph:
	int numnewedges = 0;
	for(int x = 1; x <= numvertices; x++) {
		int self = mapping[x];
		for(set<int>::iterator iter = sets[x]->begin(); iter != sets[x]->end(); ++iter) {
			int oldneighbor = *iter;
			if(oldneighbor > x) {
				//Note to self; each vertex has at most ONE old edge
				int neighbor = mapping[oldneighbor];
				while(newsets[neighbor]->size() > 0) {
					neighbor++;
				}
				while(newsets[self]->size() > 0) {
					self++;
				}
				
				if(neighbor >= mapping[oldneighbor + 1]) {
					cerr << "SANITY CHECK ERROR ~132: no valid vertex.\n";
				}
				if(self >= mapping[x + 1]) {
					cerr << "SANITY CHECK ERROR ~138: no valid vertex.\n";
				}
				
				newsets[neighbor]->push_back(self);
				newsets[self]->push_back(neighbor);
				numnewedges++;
			}
		}
        free(sets[x]);
	}
	free(sets);
	
	//Error check:
	for(int x = 1; x <= numnewvertices; x++) {
		if(newsets[x]->size() != 1) {
			cerr << "SANITY CHECK ERROR ~150: node has " << newsets[x]->size() << " new edges.\n";
		}
	}
	
	//Connect 'rings' in new graph:
#ifdef USE_DEGREE_THRESHOLD
    cerr << "DEGREE_THRESHOLD is " << DEGREE_THRESHOLD << endl;
#endif
#ifdef ADA_DEGREE_THRESHOLD
    cerr << "Adaptive DEGREE_THRESHOLD is " << degree_threshold << " (" << max_degree<< ")" << endl;
#endif
    int num_fatv = 0;
	for(int x = 1; x <= numvertices; x++) {
		if(mapping[x+1] == mapping[x]) {
			continue;
		}
		
#ifdef USE_DEGREE_THRESHOLD
        if(mapping[x+1] - mapping[x] >= DEGREE_THRESHOLD)
            num_fatv++;
        if(mapping[x+1] - mapping[x] < DEGREE_THRESHOLD)
        {
#endif
#ifdef ADA_DEGREE_THRESHOLD
        if(mapping[x+1] - mapping[x] > degree_threshold)
            num_fatv++;
        if(mapping[x+1] - mapping[x] <= degree_threshold)
        {
#endif
		for(int y = mapping[x]; y < mapping[x+1] - 1; y++) {
			newsets[y]->push_back(y + 1);
			newsets[y + 1]->push_back(y);
			numnewedges++;
		}
		
		if(mapping[x+1] > mapping[x] + 2) {
			newsets[mapping[x]]->push_back(mapping[x+1]-1);
			newsets[mapping[x+1]-1]->push_back(mapping[x]);
			numnewedges++;
		}
#ifdef USE_DEGREE_THRESHOLD
        }
#endif
#ifdef ADA_DEGREE_THRESHOLD
        }
#endif
	}
    cerr << "Number of large degree nodes is " << num_fatv << " / " << numvertices << endl;
	
	//Output new graph:
	ofstream newfile(newfileName);
	ofstream weightfile(weightfileName);
	newfile << numnewvertices << " " << numnewedges << endl;
	weightfile << numnewvertices << " " << numnewedges << " 001" << endl;
	for(int v = 1; v <= numnewvertices; v++) {
		newfile << " ";
		for(vector<int>::iterator iter = newsets[v]->begin(); iter != newsets[v]->end(); ++iter) {
			newfile << *iter;
			newfile << " ";
            weightfile << *iter << " ";
            if(iter == newsets[v]->begin())
                weightfile << "1000 ";
            else
                weightfile << "1 ";
		}
		newfile << endl;
        weightfile << endl;
	}
	newfile.close();
	weightfile.close();
	
	free(mapping);
	free(newsets);
}

