#include <fstream>
#include <iostream>
#include <set>
#include <vector>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "transform.hpp"

using namespace std;

void transform(set<int>** sets, int numvertices, int numedges, vector<int>** & newsets, int* & mapping, int & numnewvertices, int & numnewedges) {
	//Output old graph:
    int max_degree = 0;
	for(int v = 1; v <= numvertices; v++) {
        if((int) sets[v]->size() > max_degree)
            max_degree = sets[v]->size();
	}

    // identify graph feature
    // produce degree histogram
    int * histogram = new int[max_degree+1];
    for(int i = 0; i < max_degree+1; i++)
        histogram[i] = 0;
    for(int i = 0; i < numvertices; i++)
    {
        assert((int) sets[i+1]->size() <= max_degree);
        histogram[sets[i+1]->size()]++;
    }
    int sum_now = 0;
    int degree_threshold = -1;
    for(int i = 0; i < max_degree+1; i++)
    {
        sum_now += histogram[i];
        if( (double)sum_now / (double)numvertices >= 0.998 ) {
            degree_threshold = i;
            break;
        }
    }
    if(degree_threshold == -1){
        assert(sum_now == numvertices);
        degree_threshold = max_degree;
    }
	
	//Calculate mapping:
	int counter = 1;
	mapping = (int*) malloc(sizeof(int) * (numvertices+2));
	for(int x = 1; x <= numvertices; x++) {
		mapping[x] = counter;
		int neighbors = sets[x]->size();
		if(neighbors > 0) {
			counter += neighbors;
		}
	}
	mapping[numvertices+1] = counter;
	
	//Get new vertex count:
	numnewvertices = numedges * 2;
	
	//Initialize new sets:
	newsets = (vector<int>**) malloc(sizeof(vector<int>*) * (numnewvertices+1));
	for(int x = 1; x <= numnewvertices; x++) {
		newsets[x] = new vector<int>;
	}
	
	//Add old edges to new graph:
	numnewedges = 0;
	for(int x = 1; x <= numvertices; x++) {
		int self = mapping[x];
		for(set<int>::iterator iter = sets[x]->begin(); iter != sets[x]->end(); ++iter) {
			int oldneighbor = *iter;
			if(oldneighbor > x) {
				//Note to self; each vertex has at most ONE old edge
				int neighbor = mapping[oldneighbor];
				while(newsets[neighbor]->size() > 0) {
					neighbor++;
				}
				while(newsets[self]->size() > 0) {
					self++;
				}
				
				if(neighbor >= mapping[oldneighbor + 1]) {
					cerr << "SANITY CHECK ERROR ~132: no valid vertex.\n";
				}
				if(self >= mapping[x + 1]) {
					cerr << "SANITY CHECK ERROR ~138: no valid vertex.\n";
				}
				
				newsets[neighbor]->push_back(self);
				newsets[self]->push_back(neighbor);
				numnewedges++;
			}
		}
        delete sets[x];
	}
	free(sets);
	
	//Error check:
	for(int x = 1; x <= numnewvertices; x++) {
		if(newsets[x]->size() != 1) {
			cerr << "SANITY CHECK ERROR ~150: node has " << newsets[x]->size() << " new edges.\n";
		}
	}
	
	//Connect 'rings' in new graph:
    int num_fatv = 0;
	for(int x = 1; x <= numvertices; x++) {
		if(mapping[x+1] == mapping[x]) {
			continue;
		}
		
		for(int y = mapping[x]; y < mapping[x+1] - 1; y++) {
			newsets[y]->push_back(y + 1);
			newsets[y + 1]->push_back(y);
			numnewedges++;
		}
		
		if(mapping[x+1] > mapping[x] + 2) {
			newsets[mapping[x]]->push_back(mapping[x+1]-1);
			newsets[mapping[x+1]-1]->push_back(mapping[x]);
			numnewedges++;
		}
	}
    cerr << "Number of large degree nodes is " << num_fatv << " / " << numvertices << endl;
	
	/*
	//Print output:
	cout << "1) Array newsets contains " << numnewvertices << "+1 int vectors:\n";
	for(int x = 1; x <= numnewvertices; x++) {
		cout << " newsets[" << x << "]: ";
		int sz = newsets[x]->size();
		for(int y = 0; y < sz; y++) {
			cout << newsets[x]->at(y) << ", ";
		}
		cout << "\n";
	}
	cout << "2) Array mapping contains " << numvertices << "+2 integers. Starting from index 1:\n ";
	for(int x = 1; x <= numvertices+2; x++) {
		cout << mapping[x] << ", ";
	} cout << "\n";
	cout << "3) numnewvertices = " << numnewvertices << "\n";
	cout << "4) numnewedges = " << numnewedges << "\n";
	*/
}
