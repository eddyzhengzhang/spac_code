#include <fstream>
#include <iostream>
#include <set>
#include <vector>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "vertPart.hpp"
#include "readFile.hpp"
#include "repartition.hpp"
#include "transform.hpp"

using namespace std;

int main(int argc, char * argv[]) {
	if(argc != 4) {
		cerr << "Expected input: " << argv[0] << " -graph <input file> <# partitions>" << endl;
		cerr << "Expected input: " << argv[0] << " -mat <input file> <# partitions>" << endl;
		exit(0);
	}
	
	clock_t clock1, clock2;
	double timeDiff;
	
	int numparts = atoi(argv[3]);
	
	int numvertices;
	int numedges;
	set<int> ** sets;
	int numrows;
	int numcols;
	
	if(!strcmp(argv[1], "-graph")) {
		clock1 = clock();
		readGraph(argv[2], sets, numvertices, numedges);
		clock2 = clock();
		timeDiff = (double)(clock2 - clock1)*1000 / CLOCKS_PER_SEC;
		cerr << "TIME FUNCTION 1 input (ms): " << timeDiff << "\n";
	} else if(!strcmp(argv[1], "-mat")) {
		clock1 = clock();
		readMatrix(argv[2], sets, numvertices, numedges, numrows, numcols);
		clock2 = clock();
		timeDiff = (double)(clock2 - clock1)*1000 / CLOCKS_PER_SEC;
		cerr << "TIME FUNCTION 1 input (ms): " << timeDiff << "\n";
	} else {
		cerr << "FATAL ERROR: unexpected input.\n";
		exit(1);
	}
	
	vector<int> ** newsets;
	int * mapping;
	int numnewvertices;
	int numnewedges;
	clock1 = clock();
	transform(sets, numvertices, numedges, newsets, mapping, numnewvertices, numnewedges);
	clock2 = clock();
	timeDiff = (double)(clock2 - clock1)*1000 / CLOCKS_PER_SEC;
	cerr << "TIME FUNCTION 2 transform (ms): " << timeDiff << "\n";
	
	///////////////////////////
	//call partGraphKway code//
	///////////////////////////
	clock1 = clock();
	
	Edge * edges = (Edge *) malloc(sizeof(Edge) * numnewedges * 2);
	int edgesSeen = 0;
	//Read in all neighbors for current vertex:
	for(int v = 1; v <= numnewvertices; v++) {
		for(vector<int>::iterator iter = newsets[v]->begin(); iter != newsets[v]->end(); ++iter) {
			edges[edgesSeen].head = v - 1;
			edges[edgesSeen].tail = *iter - 1;
			edgesSeen++;
		}
	}
	
	//Call metis caller:
	int * partitionList;
	partitionList = vertexpart(edges, numnewvertices, edgesSeen, numparts);
	
	free(edges);
	
	clock2 = clock();
	timeDiff = (double)(clock2 - clock1)*1000 / CLOCKS_PER_SEC;
	cerr << "TIME FUNCTION 3 partition (ms): " << timeDiff << "\n";
	
	///////////////////////////
	///////////////////////////
	///////////////////////////
	
	///////////////////////////
	//repartition code below://
	///////////////////////////
	clock1 = clock();
	repartition(numvertices, numedges, numnewvertices, numnewedges, numparts, newsets, partitionList, mapping);
	clock2 = clock();
	timeDiff = (double)(clock2 - clock1)*1000 / CLOCKS_PER_SEC;
	cerr << "TIME FUNCTION 4 repartition&output (ms): " << timeDiff << "\n";
	
	///////////////////////////
	///////////////////////////
	///////////////////////////
	
	for(int x = 1; x <= numnewvertices; x++) {
		delete newsets[x];
	}
	free(newsets);
	free(partitionList);
	free(mapping);
}
